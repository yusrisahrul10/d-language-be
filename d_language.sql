create database d_language;

use d_language;

create table language(
	language_id int IDENTITY(1,1) primary key,
	name VARCHAR(50) NOT NULL,
	description TEXT NOT NULL,
	flag_img VARCHAR(50),
	banner_img VARCHAR(50),
);

create table users(
	email VARCHAR(50) NOT NULL PRIMARY KEY,
	name varchar(50) not null,
	password_hash VARBINARY(256) NOT NULL,
	password_salt VARBINARY(256) NOT NULL,
	role int NOT NULL,
	active int NOT NULL
);

CREATE TABLE course(
	course_id INT IDENTITY(1,1) PRIMARY KEY,
	language_id INT NOT NULL,
	name VARCHAR(50) NOT NULL,
	description TEXT NOT NULL,
	price int NOT NULL,
	course_img varchar(50) NOT NULL,
	
	FOREIGN KEY (language_id) REFERENCES language (language_id) 
);

create table checkout(
	checkout_id int IDENTITY(1,1) primary key,
	course_id int not null,
	email varchar(50) not null,
	qty int not null,
	price int not null,
	schedule_date VARCHAR(50) NOT NULL,

	FOREIGN KEY (course_id) REFERENCES course (course_id),
	FOREIGN KEY (email) REFERENCES users (email) 
);

create table invoice(
	invoice_id int IDENTITY(1,1)  primary key,
	email varchar(50) not null,
	total_qty int not null,
	checkout_date varchar(50) not null,
	total_price int not null,
	payment_method varchar(50) not null,

	FOREIGN KEY (email) REFERENCES users (email) 
);

create table invoice_detail(
	invoice_detail_id int IDENTITY(1,1) primary key,
	invoice_id int not null,
	course_name varchar(50) not null,
	language_name varchar(50) not null,
	course_schedule varchar(50) not null,
	price int not null

	FOREIGN KEY (invoice_id) REFERENCES invoice (invoice_id) 
);

create table token(
	pk_token_id int IDENTITY(1,1) PRIMARY KEY,
	token varchar(50) not null,
	expire_date varchar(50),
	usage_type varchar(50) not null,
	email varchar(50) not null,
);

create table payments(
	payments_id int IDENTITY(1,1) PRIMARY KEY,
	name varchar(50) not null,
	payment_img varchar(max) not null,
	status int not null,
)

create table my_class(
	my_class_id int IDENTITY(1,1) PRIMARY KEY,
	email varchar(50) not null,
	course_img varchar(max) NOT NULL,
	language_name varchar(50) NOT NULL,
	course_name varchar(50) NOT NULL,
	schedule varchar(50) NOT NULL,

	FOREIGN KEY (email) REFERENCES users (email)
)