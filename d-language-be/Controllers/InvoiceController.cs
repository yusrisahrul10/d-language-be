﻿using d_language_be.Logics;
using d_language_be.Models.Request.Admin;
using d_language_be.Models.Request.Invoice;
using d_language_be.Models.Response.Invoice;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace d_language_be.Controllers
{
    [Route("api/invoice")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        [HttpPost]
        [Route("add")]
        [Authorize]
        public ActionResult AddInvoice([FromBody] Invoice body)
        {
            try
            {
                string message = InvoiceLogic.AddInvoice(body);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("get")]
        [Authorize]
        public ActionResult GetInvoiceByEmail([FromBody] UserRequest body)
        {
            try
            {
                List<InvoiceResponse> response = new List<InvoiceResponse>();

                response = InvoiceLogic.GetInvoiceByEmail(body);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("detail/get")]
        //[Authorize]
        public ActionResult GetDetailInvoiceById([FromBody] UserInvoice body)
        {
            try
            {
                InvoiceDetailResponse response = new InvoiceDetailResponse();

                response = InvoiceLogic.GetDetailInvoiceById(body);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("all/get")]
        [Authorize]
        public ActionResult GetAllInvoice()
        {
            try
            {
                List<InvoiceAllResponse> response = new List<InvoiceAllResponse>();

                response = InvoiceLogic.GetAllInvoice();

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
