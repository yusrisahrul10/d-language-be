﻿using d_language_be.Logics;
using d_language_be.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace d_language_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetCourses()
        {
            try
            {
                List<Course> courses = new List<Course>();
                courses = CourseLogic.GetCourses();
                return Ok(courses);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
            
        }

        [HttpGet("{id}")]
        public IActionResult GetCourse(int id)
        {
            try
            {
                List<Course> course = new List<Course>();
                course = CourseLogic.GetCourse(id);
                return Ok(course);
            }
            catch  (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet]
        [Route("category/{category}")]
        public IActionResult GetCourseByCategory(string category)
        {
            try
            {
                List<Course> course = CourseLogic.GetCourseByCategory(category);
                return Ok(course);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet]
        [Route("CourseBetween")]
        public IActionResult GetCourseBetween(string category, int id)
        {
            try
            {
                List<Course> course = CourseLogic.GetCourseBetween(category, id);
                return Ok(course);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }
    }
}
