﻿using d_language_be.Logics;
using d_language_be.Models;
using d_language_be.Models.Auth;
using d_language_be.Models.Request.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace d_language_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckoutController : ControllerBase
    {
        [HttpGet]
        [Authorize]
        public IActionResult GetAllData()
        {
            try
            {
                List<Checkout> result = CheckoutLogic.GetAllData();
                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPost]
        [Route("get-data-user-checkout")]
        [Authorize]
        public IActionResult GetDataByEmail([FromBody] CheckoutDTO body)
        {
            try
            {
                List<CheckoutDTO> list = CheckoutLogic.GetDataByEmail(body);
                return Ok(list);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }

        }
        [HttpGet]
        [Route("get-all-user-checkout")]
        [Authorize]
        public IActionResult GetDataAllUser()
        {
            try
            {
                List<CheckoutDTO> list = CheckoutLogic.GetDataAllUser();
                return Ok(list);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                return StatusCode(500, "Internal Server Error");
            }

        }

        [HttpPost]
        [Authorize]
        public IActionResult PostData(Checkout data)
        {
            try
            {
                string result = CheckoutLogic.PostData(data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                string errorMessage = $"Error: {ex.Message}";
                Console.WriteLine(errorMessage);
                return BadRequest(errorMessage);
            }
        }


        [HttpDelete]
        [Route("delete")]
        [Authorize]
        public IActionResult Delete(int id)
        {
            try
            {
                string result = CheckoutLogic.DeleteCheckout(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                string errorMessage = $"Error: {ex.Message}";
                Console.WriteLine(errorMessage);
                return BadRequest(errorMessage);
            }
            
        }
    }
}
