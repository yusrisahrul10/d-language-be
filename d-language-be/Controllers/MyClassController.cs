﻿using d_language_be.Logics;
using d_language_be.Models.Request.Admin;
using d_language_be.Models.Response.MyClass;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace d_language_be.Controllers
{
    [Route("api/my-class")]
    [ApiController]
    public class MyClassController : ControllerBase
    {
        [HttpPost]
        [Route("get")]
        [Authorize]
        public ActionResult GetMyClassByEmail([FromBody] UserRequest body)
        {
            try
            {
                List<ClassResponse> response = new List<ClassResponse>();
                response = MyClassLogic.GetClassByEmail(body);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
