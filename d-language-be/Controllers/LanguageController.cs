﻿using d_language_be.Logics;
using d_language_be.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace d_language_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetLanguages()
        {
            List<Language> result = LanguageLogic.GetLanguages();
            return Ok(result);
        }

        [HttpGet("{category}")]
        public IActionResult GetLanguage(string category)
        {
            List<Language> result = LanguageLogic.GetLanguage(category);
            return Ok(result);
        }
    }
}
