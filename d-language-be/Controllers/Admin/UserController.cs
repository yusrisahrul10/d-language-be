﻿using d_language_be.Logics.Admin;
using d_language_be.Models;
using d_language_be.Models.Request.Admin;
using d_language_be.Models.Response.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace d_language_be.Controllers.Admin
{
    [Route("api/admin")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet]
        [Route("user/get")]
        [Authorize(Roles = "1")]
        public ActionResult GetUser()
        {
            try
            {
                List<UserResponse> response = new List<UserResponse>();
                response = AdminLogic.GetAllUser();

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("user/getUserActiveOnly")]
        [Authorize(Roles = "1")]
        public ActionResult getUserActiveOnly()
        {
            try
            {
                List<UserResponse> response = new List<UserResponse>();
                response = AdminLogic.getUserActiveOnly();

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("user/getMemberOnly")]
        [Authorize(Roles = "1")]
        public ActionResult getMemberOnly()
        {
            try
            {
                List<UserResponse> response = new List<UserResponse>();
                response = AdminLogic.getMemberOnly();

                return Ok(response);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("user/add")]
        [Authorize(Roles = "1")]
        public ActionResult AddUser([FromBody] AddUserRequest body)
        {
            try
            {
                string message = AdminLogic.AddUser(body);

                return Ok(message);
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("user/edit")]
        [Authorize(Roles = "1")]
        public ActionResult EditUser([FromBody] EditUserRequest body)
        {
            try
            {
                string message = AdminLogic.EditUser(body);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
