using d_language_be.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Data.SqlClient;
using System.Data;
using d_language_be.Logics.Auth;
using d_language_be.Models.Auth;
using Microsoft.AspNetCore.Authorization;

namespace d_language_be.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpPost]
        [Route("registeruser")]
        public ActionResult RegisterUser([FromBody] UserDTO body)
        {
            try
            {
                string message = AuthLogic.RegisterUser(body);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("verifyuser")]
        public ActionResult VerifyUser(string token)
        {
            try
            {
                string message = AuthLogic.VerifyUser(token);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("loginuser")]
        public ActionResult LoginUser([FromBody] UserDTO body)
        {
            try
            {
                string message = AuthLogic.LoginUser(body);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("resetpass")]
        public ActionResult ResetPassword([FromBody] EmailBody body)
        {
            try
            {
                string message = AuthLogic.ResetPassword(body);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("createpass")]
        public ActionResult CreateNewPassword(string token, [FromBody] CreateNewPass body)
        {
            try
            {
                string message = AuthLogic.CreateNewPassword(token, body);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("verifytokenreset")]
        public ActionResult VerifyTokenReset(string token)
        {
            try
            {
                string message = AuthLogic.VerifyTokenReset(token);

                return Ok(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("validatetokenjwt")]
        [Authorize]
        public ActionResult TokenCheck([FromBody] EmailBody body)
        {
            try
            {
                AuthLogic.ValidateUserActive(body);
                return StatusCode(200, "Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

}
