﻿using d_language_be.Logics;
using d_language_be.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace d_language_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        [HttpGet]
        [Authorize]
        public IActionResult Get()
        {
            List<Payment> result = new List<Payment>();
            result = PaymentLogic.GetPayment();

            return Ok(result);
        }

        [HttpGet]
        [Route("ActiveOnly")]
        public IActionResult GetPaymentActiveOnly()
        {
            List<Payment> result = new List<Payment>();
            result = PaymentLogic.GetPaymentActiveOnly();

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            List<Payment> result = new List<Payment>();
            result = PaymentLogic.GetPaymentById(id);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Post([FromBody] Payment body)
        {
            try
            {
                string result = PaymentLogic.AddPayments(body);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [HttpPost]
        [Route("update")]
        public IActionResult Update(int id, [FromBody] Payment body)
        {
            try
            {
                string result = PaymentLogic.UpdatePayment(id, body);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
