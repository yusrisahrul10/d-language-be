﻿using d_language_be.Models;
using d_language_be.Models.Request.Admin;
using Microsoft.Extensions.Configuration;
using MySqlX.XDevAPI.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace d_language_be.Logics
{
    public class CheckoutLogic
    {
        private static string connectionString = "";
        private static string baseUrl = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("Default");
            baseUrl = configuration["fronturl"];
        }

        public static List<Checkout> GetAllData()
        {
            List<Checkout> result = new List<Checkout>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT * FROM checkout";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            Checkout checkout = new Checkout()
                            {
                                checkoutId = Convert.ToInt32(dr["checkout_id"]),
                                courseId = Convert.ToInt32(dr["course_id"]),
                                email = dr["email"].ToString(),
                                qty = Convert.ToInt32(dr["qty"]),
                                scheduleDate = dr["schedule_date"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                payedAt = dr["payyed_at"].ToString()
                            };
                            result.Add(checkout);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<CheckoutDTO> GetDataByEmail(CheckoutDTO body)
        {
            List<CheckoutDTO> result = new List<CheckoutDTO>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT ch.checkout_id ,c.name AS course_name, c.price ,c.course_img ,l.name AS category ,ch.email ,ch.schedule_date FROM [d_language].[dbo].[course] c JOIN [d_language].[dbo].[language] l ON c.[language_id] = l.[language_id] JOIN [d_language].[dbo].[checkout] ch ON c.[course_id] = ch.[course_id] WHERE ch.email = @email ORDER BY ch.checkout_id DESC";


                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@email", body.email);

                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {

                            CheckoutDTO checkout = new CheckoutDTO()
                            {
                                checkoutId = Convert.ToInt32(dr["checkout_id"]),
                                courseName = dr["course_name"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                courseImg = dr["course_img"].ToString(),
                                category = dr["category"].ToString(),
                                email = dr["email"].ToString(),
                                scheduleDate = dr["schedule_date"].ToString(),
                            };
                            result.Add(checkout);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<CheckoutDTO> GetDataAllUser()
        {
            List<CheckoutDTO> result = new List<CheckoutDTO>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT ch.checkout_id ,c.name AS course_name, c.price ,c.course_img \r\n\t,l.name AS category ,u.name AS username ,ch.schedule_date, \r\n\tpayyed_at FROM [d_language].[dbo].[course] c \r\n\tJOIN [d_language].[dbo].[language] l ON c.[language_id] = l.[language_id] \r\n\tJOIN [d_language].[dbo].[checkout] ch ON c.[course_id] = ch.[course_id] \r\n\tJOIN [d_language].[dbo].[users] u ON ch.email = u.email";


                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {

                            CheckoutDTO checkout = new CheckoutDTO()
                            {
                                checkoutId = Convert.ToInt32(dr["checkout_id"]),
                                courseName = dr["course_name"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                courseImg = dr["course_img"].ToString(),
                                category = dr["category"].ToString(),
                                email = dr["username"].ToString(),
                                scheduleDate = dr["schedule_date"].ToString(),
                            };
                            result.Add(checkout);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static string PostData(Checkout body)
        {
            try
            {
                if (string.IsNullOrEmpty(body.scheduleDate))
                {
                    throw new Exception("Schedule date is required.");
                }

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    // Check if the course and schedule data already exist
                    string checkQuery = "SELECT COUNT(*) FROM [checkout] WHERE [course_id] = @courseId AND [schedule_date] = @scheduleDate AND [email] = @email";
                    using (SqlCommand checkCmd = new SqlCommand(checkQuery, conn))
                    {
                        checkCmd.Parameters.AddWithValue("@courseId", body.courseId);
                        checkCmd.Parameters.AddWithValue("@scheduleDate", body.scheduleDate);
                        checkCmd.Parameters.AddWithValue("@email", body.email);
                        int existingDataCount = (int)checkCmd.ExecuteScalar();

                        if (existingDataCount > 0)
                        {
                            throw new Exception("Course and schedule data already exist.");
                        }
                    }

                    string checkMyClassQuery = "SELECT COUNT(*) FROM [my_class] WHERE [course_name] = @course_name AND [schedule] = @scheduleDate AND [email] = @email";
                    using (SqlCommand checkCmd = new SqlCommand(checkMyClassQuery, conn))
                    {
                        checkCmd.Parameters.AddWithValue("@course_name", body.name);
                        checkCmd.Parameters.AddWithValue("@scheduleDate", body.scheduleDate);
                        checkCmd.Parameters.AddWithValue("@email", body.email);
                        int existingDataCount = (int)checkCmd.ExecuteScalar();

                        if (existingDataCount > 0)
                        {
                            throw new Exception("Course and schedule already exist in your class.");
                        }
                    }

                    // Insert the new data if it doesn't exist
                    string insertQuery = "INSERT INTO [checkout] ([course_id], [email], [qty], [price], [schedule_date]) " +
                                         "VALUES (@courseId, @email, @qty, @price, @schedule_date)";
                    using (SqlCommand cmd = new SqlCommand(insertQuery, conn))
                    {
                        SqlParameter[] parameters =
                        {
                    new SqlParameter("@courseId", SqlDbType.Int) { Value = body.courseId },
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email },
                    new SqlParameter("@qty", SqlDbType.Int) { Value = body.qty },
                    new SqlParameter("@price", SqlDbType.Int) { Value = body.price },
                    new SqlParameter("@schedule_date", SqlDbType.VarChar) { Value = body.scheduleDate }
                };

                        cmd.Parameters.AddRange(parameters);
                        cmd.ExecuteNonQuery();
                        conn.Close();

                        return "Successfully added to Cart";
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw new Exception("An SQL error occurred while processing the request.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }
        }


        public static string DeleteCheckout(int id)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "DELETE FROM checkout WHERE checkout_id = @Id";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);

                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            return "Data has been successfully deleted";
                        }
                        else
                        {
                            throw new Exception("Error: Data not found");
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }
        }

    }
}
