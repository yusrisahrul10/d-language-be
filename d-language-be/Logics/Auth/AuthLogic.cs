﻿using d_language_be.Models;
using System.Data.SqlClient;
using System.Data;
using System.Transactions;
using System;
using System.Security.Claims;
using d_language_be.Models.Auth;

namespace d_language_be.Logics.Auth
{
    public class AuthLogic
    {
        private static string connectionString = "";
        private static string baseUrl = "";
        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
            baseUrl = configuration["fronturl"];
        }

        public static string LoginUser(UserDTO body)
        {
            try
            {
                if (String.IsNullOrEmpty(body.email))
                {
                    throw new Exception("email cant be empty");
                }

                if (String.IsNullOrEmpty(body.password))
                {
                    throw new Exception("password cant be empty");
                }

                string query = "Select top 1 * from users where [email] = @email and active = 1";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" }
                };

                DataTable users = CRUD.ExecuteQuery(query, sqlParams);

                if (users.Rows.Count == 0)
                {
                    throw new Exception("User not found or not yet validated");
                }

                byte[] passwordHash = Array.Empty<byte>();
                byte[] passwordSalt = Array.Empty<byte>();
                int roleuser = 0;

                foreach (DataRow userRow in users.Rows)
                {
                    passwordHash = userRow["password_hash"] == DBNull.Value ? Array.Empty<byte>() : (byte[])userRow["password_hash"];
                    passwordSalt = userRow["password_salt"] == DBNull.Value ? Array.Empty<byte>() : (byte[])userRow["password_salt"];
                    roleuser = userRow["role"] == DBNull.Value ? 0 : (int)userRow["role"];
                }

                bool isValid = CryptoLogic.CompareHastVsString(body.password, passwordHash, passwordSalt);

                if (!isValid)
                {
                    throw new Exception("Wrong Credential");
                }

                //create jwt token

                string tokenJwt = JwtTokenLogic.GenerateJwtToken(new[]
                {
                    new Claim("email", body.email ?? ""),
                    new Claim(ClaimTypes.Role,  roleuser.ToString() ?? "0")
                }
                );

                return tokenJwt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string RegisterUser(UserDTO user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        if (String.IsNullOrEmpty(user.name))
                        {
                            throw new Exception("name cant be empty");
                        }

                        if (String.IsNullOrEmpty(user.password))
                        {
                            throw new Exception("password cant be empty");
                        }

                        if (String.IsNullOrEmpty(user.email))
                        {
                            throw new Exception("email cant be empty");
                        }

                        string query = "Select top 1 * from users where [email] = @email";
                        SqlParameter[] sqlParams = new SqlParameter[]
                        {
                            new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" }
                        };

                        DataTable users = CRUD.ExecuteQuery(query, sqlParams);

                        if (users.Rows.Count > 0)
                        {
                            throw new Exception("User Already Registered");
                        }


                        byte[] passwordHash = Array.Empty<byte>();
                        byte[] passwordSalt = Array.Empty<byte>();

                        //(passwordHash,passwordSalt) = CryptoLogic.GenerateHash(body.password);
                        CryptoLogic.GenerateHash(user.password, out passwordHash, out passwordSalt);

                        cmd.CommandText = "INSERT INTO users([email], [name], password_hash, password_salt, role, active) VALUES (@email, @name, @password_hash, @password_salt, 2,0)";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar) { Value = user.name ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash });
                        cmd.Parameters.Add(new SqlParameter("@password_salt", SqlDbType.VarBinary) { Value = passwordSalt });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        string token = Guid.NewGuid().ToString();

                        cmd.CommandText = "Insert into token (token, expire_date, usage_type,email) values (@token,null,'register',@email)";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar) { Value = token });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();



                        string sendTo = user.email ?? "";
                        string subject = "Activate your account";
                        string activationUrl = baseUrl + "/verifyuser/" + token;

                        string emailBody = @"
                        <html>
                        <head>
                            <style>
                                body {
                                    font-family: 'Montserrat', sans-serif;
                                    background-color: #f5f5f5;
                                    margin: 0;
                                    padding: 0;
                                }

                                .container {
                                    max-width: 600px;
                                    margin: 0 auto;
                                    background-color: #ffffff;
                                    border-radius: 20px;
                                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                                    padding: 40px;
                                }

                                .logo {
                                    text-align: center;
                                    margin-bottom: 40px;
                                }

                                .logo img {
                                    max-width: 200px;
                                    height: auto;
                                }
      
                                .logo h2 {
                                    margin-top: 0;
                                }

                                h2 {
                                    color: #226957;
                                    font-size: 24px;
                                    margin-bottom: 20px;
                                }

                                p {
                                    color: #666666;
                                    font-size: 16px;
                                    line-height: 1.5;
                                    margin-bottom: 16px;
                                }

                                a.button {
                                    display: inline-block;
                                    padding: 12px 24px;
                                    background-color: #226957;
                                    color: #ffffff;
                                    text-decoration: none;
                                    border-radius: 4px;
                                    transition: background-color 0.3s ease;
                                }

                                a.button:hover {
                                    background-color: #EA9E1F;
                                }

                                .footer {
                                    margin-top: 40px;
                                    text-align: center;
                                    font-size: 14px;
                                    color: #999999;
                                }
                            </style>
                        </head>
                        <body>
                            <div class=""container"">
                                <div class=""logo"">
                                    <img src=""https://i.imgur.com/rZtKzHH.png"" alt=""Company Logo"">
                                    <h2>D'Language</h2>
                                </div>
                                <h2>Welcome to D'Language!</h2>
                                <p>Thank you for joining our community. To activate your account, please click the activation link below:</p>
                                <a class=""button"" href=""" + activationUrl + @""">Activate Account</a>
                                <p>If you didn't sign up for an account on D'Language, please ignore this email.</p>
                                <p>If you have any questions or need further assistance, feel free to contact our support team. We're here to help!</p>
                                <p>Thank you and enjoy your experience with D'Language!</p>
                                <p class=""footer"">Best regards,<br>D'Language Team</p>
                            </div>
                        </body>
                        </html>";

                        EmailLogic.SendEmail(sendTo, subject, emailBody);

                        cmd.Transaction.Commit();
                        //conn.Close();

                        return "Success Register. Email Activation Sent";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        throw ex;
                    } finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static string VerifyUser(string token)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        cmd.CommandText = "Select top 1 email from token where token = @token and usage_type = 'register'";
                        cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar) { Value = token ?? "" });

                        object emailObject = cmd.ExecuteScalar();
                        string email = emailObject == DBNull.Value ? "" : (string)emailObject;
                        cmd.Parameters.Clear();

                        if (String.IsNullOrEmpty(email))
                        {
                            throw new Exception("Activation link not valid");
                        }

                        cmd.CommandText = "Update users set active = 1 where email = @email";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = email ?? "" });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandText = "delete from Token where email = @email and usage_type = 'register'";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = email ?? "" });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.Transaction.Commit();
                        conn.Close();

                        return "Success Activation";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        conn.Close();
                        throw ex;
                    }
                }
            }
        }

        public static string ResetPassword(EmailBody user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        if (String.IsNullOrEmpty(user.email))
                        {
                            throw new Exception("email cant be empty");
                        }

                        cmd.CommandText = "Select top 1 * from users where [email] = @email and active = 1";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });

                        object emailObject = cmd.ExecuteScalar();
                        string email = emailObject == DBNull.Value ? "" : (string)emailObject;
                        cmd.Parameters.Clear();

                        if (String.IsNullOrEmpty(email))
                        {
                            throw new Exception("User not found or not yet activated");
                        }

                        string token = Guid.NewGuid().ToString();

                        cmd.CommandText = "Insert into token (token, expire_date, usage_type,email) values (@token,null,'reset',@email)";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar) { Value = token });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        string sendTo = user.email ?? "";
                        string subject = "Reset your password";
                        string verificationUrl = baseUrl + "/new-password/" + token;

                        string emailBody = @"
                        <head>
                            <style>
                                body {
                                    font-family: 'Montserrat', sans-serif;
                                    background-color: #f5f5f5;
                                    margin: 0;
                                    padding: 0;
                                }

                                .container {
                                    max-width: 600px;
                                    margin: 0 auto;
                                    background-color: #ffffff;
                                    border-radius: 20px;
                                    border-box: 1px solid #ddd ;
                                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                                    padding: 40px;
                                }

                                .logo {
                                    text-align: center;
                                    margin-bottom: 40px;
                                }

                                .logo img {
                                    max-width: 200px;
                                    height: auto;
                                }
      
                                .logo h2 {
                                    margin-top: 0;
                                }

                                h2 {
                                    color: #226957;
                                    font-size: 24px;
                                    margin-bottom: 20px;
                                }

                                p {
                                    color: #666666;
                                    font-size: 16px;
                                    line-height: 1.5;
                                    margin-bottom: 16px;
                                }

                                a.button {
                                    display: inline-block;
                                    padding: 12px 24px;
                                    background-color: #226957;
                                    color: #ffffff;
                                    text-decoration: none;
                                    border-radius: 4px;
                                    transition: background-color 0.3s ease;
                                }

                                a.button:hover {
                                    background-color: #EA9E1F;
                                }

                                .footer {
                                    margin-top: 40px;
                                    text-align: center;
                                    font-size: 14px;
                                    color: #999999;
                                }
                            </style>
                        </head>
                        <body>
                            <div class=""container"">
                                <div class=""logo"">
                                    <img src=""https://i.imgur.com/rZtKzHH.png"" alt=""Company Logo"">
                                    <h2>D'Language</h2>
                                </div>
                                <h2>Dear Customer</h2>
                                <p>We hope this email finds you well. We wanted to inform you that you have requested a password reset for your account. No need to worry, we've got you covered!</p>
                                <p>To reset your password, simply click the link below:</p>
                                <a class=""button"" href=""" + verificationUrl + @""">Reset password</a>
                                <p>If you didn't request a password reset, please ignore this email. Your account is safe and secure.</p>
                                <p>If you have any questions or need further assistance, feel free to contact our support team. We're here to help!</p>
                                <p>Thank you for being a valued member of our community.</p>
                                <p class=""footer"">Best regards,<br>D'Language Team</p>
                            </div>
                        </body>";

                                                EmailLogic.SendEmail(sendTo, subject, emailBody);


                        cmd.Transaction.Commit();
                        //conn.Close();

                        return "We've sent you an email. Just follow the instructions to reset your password.";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static string VerifyTokenReset(string token)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        cmd.CommandText = "Select top 1 email from token where token = @token and usage_type = 'reset'";
                        cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar) { Value = token ?? "" });

                        object emailObject = cmd.ExecuteScalar();
                        string email = emailObject == DBNull.Value ? "" : (string)emailObject;
                        cmd.Parameters.Clear();

                        if (String.IsNullOrEmpty(email))
                        {
                            throw new Exception("Reset link not valid");
                        }

                        cmd.Transaction.Commit();
                        //conn.Close();

                        return "Token is valid";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        //conn.Close();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static string ValidateUserActive(EmailBody body)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        if (String.IsNullOrEmpty(body.email))
                        {
                            throw new Exception("email cant be empty");
                        }

                        cmd.CommandText = "Select top 1 * from users where [email] = @email and active = 1";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" });

                        object emailObject = cmd.ExecuteScalar();
                        string email = emailObject == DBNull.Value ? "" : (string)emailObject;
                        cmd.Parameters.Clear();

                        if (String.IsNullOrEmpty(email))
                        {
                            throw new Exception("User not activated");
                        }

                        cmd.Transaction.Commit();
                        //conn.Close();

                        return "User is active";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static string CreateNewPassword(string token, CreateNewPass user)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        cmd.CommandText = "Select top 1 email from token where token = @token and usage_type = 'reset'";
                        cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar) { Value = token ?? "" });

                        object emailObject = cmd.ExecuteScalar();
                        string email = emailObject == DBNull.Value ? "" : (string)emailObject;
                        cmd.Parameters.Clear();

                        if (String.IsNullOrEmpty(email))
                        {
                            throw new Exception("Reset link not valid");
                        }

                        byte[] passwordHash = Array.Empty<byte>();
                        byte[] passwordSalt = Array.Empty<byte>();

                        CryptoLogic.GenerateHash(user.password, out passwordHash, out passwordSalt);

                        cmd.CommandText = "Update users set password_hash = @password_hash, password_salt = @password_salt where email = @email";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = email ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash });
                        cmd.Parameters.Add(new SqlParameter("@password_salt", SqlDbType.VarBinary) { Value = passwordSalt });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandText = "delete from Token where email = @email and usage_type = 'reset'";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = email ?? "" });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.Transaction.Commit();
                        conn.Close();

                        return "Password has been changed";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        conn.Close();
                        throw ex;
                    }
                }
            }
        }
    }
}
