﻿using d_language_be.Models;
using d_language_be.Models.Auth;
using d_language_be.Models.Request.Admin;
using d_language_be.Models.Response.Admin;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace d_language_be.Logics.Admin
{
    public class AdminLogic
    {
        private static string connectionString = "";
        private static string baseUrl = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
            baseUrl = configuration["fronturl"];
        }

        public static List<UserResponse> GetAllUser()
        {
            List<UserResponse> result = new List<UserResponse>();

            try
            {
                //if (String.IsNullOrEmpty(body.email))
                //{
                        //throw new Exception("email cant be empty");
                //}

               // string checkUserQuery = "Select top 1 * from users where [email] = @email and role = @role";
                //SqlParameter[] sqlParams = new SqlParameter[]
               // {
                 //   new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" },
                   // new SqlParameter("@role", SqlDbType.Int) { Value = 1 }
                //};

                ///DataTable users = CRUD.ExecuteQuery(checkUserQuery, sqlParams);

                ///if (users.Rows.Count == 0)
                //{
                  ///  throw new Exception("Sorry! You are not authorized to perform this action");
               // }

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    String query = "select email, name, role, active from users";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            UserResponse user = new UserResponse()
                            {
                                email = dr["email"].ToString(),
                                name = dr["name"].ToString(),
                                role = Convert.ToInt32(dr["role"]),
                                active = Convert.ToInt32(dr["active"]),
                            };
                            result.Add(user);
                        }
                    }
                    conn.Close();
                }
                //return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
            
        }

        public static List<UserResponse> getUserActiveOnly()
        {
            List<UserResponse> result = new List<UserResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    String query = "select email, name, role, active from users where active = 1 and role != 1";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            UserResponse user = new UserResponse()
                            {
                                email = dr["email"].ToString(),
                                name = dr["name"].ToString(),
                                role = Convert.ToInt32(dr["role"]),
                                active = Convert.ToInt32(dr["active"]),
                            };
                            result.Add(user);
                        }
                    }
                    conn.Close();
                }
                //return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public static List<UserResponse> getMemberOnly()
        {
            List<UserResponse> result = new List<UserResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    String query = "SELECT mc.email FROM my_class mc JOIN users u ON mc.email = u.email where u.active = 1 GROUP BY mc.email";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            UserResponse user = new UserResponse()
                            {
                                email = dr["email"].ToString(),
                               // name = dr["name"].ToString(),
                               // role = Convert.ToInt32(dr["role"]),
                               // active = Convert.ToInt32(dr["active"]),
                            };
                            result.Add(user);
                        }
                    }
                    conn.Close();
                }
                //return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public static string AddUser(AddUserRequest body)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        if (String.IsNullOrEmpty(body.name))
                        {
                            throw new Exception("name cant be empty");
                        }

                        if (String.IsNullOrEmpty(body.email_user))
                        {
                            throw new Exception("email_user cant be empty");
                        }

                        //if (String.IsNullOrEmpty(body.email_admin))
                        //{
//throw new Exception("email_admin cant be empty");
                        //}

                        //string checkUserQuery = "Select top 1 * from users where [email] = @email and role = @role";
                        //SqlParameter[] sqlParams = new SqlParameter[]
                        //{
                        //   new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email_admin ?? "" },
                        //    new SqlParameter("@role", SqlDbType.Int) { Value = 1 }
                       // };

                        //DataTable admin = CRUD.ExecuteQuery(checkUserQuery, sqlParams);

                        //if (admin.Rows.Count == 0)
                        //{
                          //  throw new Exception("Sorry! You are not authorized to perform this action");
                        //}

                        string query = "Select top 1 * from users where [email] = @email";
                        SqlParameter[] sqlParamsUsers = new SqlParameter[]
                        {
                            new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email_user ?? "" }
                        };

                        DataTable users = CRUD.ExecuteQuery(query, sqlParamsUsers);

                        if (users.Rows.Count > 0)
                        {
                            throw new Exception("User Already Registered");
                        }


                        byte[] passwordHash = Array.Empty<byte>();
                        byte[] passwordSalt = Array.Empty<byte>();

                        const string chars = "abcdefghijklmnopqrstuvwxyz";
                        var random = new Random();
                        var password = new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());

                        //(passwordHash,passwordSalt) = CryptoLogic.GenerateHash(body.password);
                        CryptoLogic.GenerateHash(password, out passwordHash, out passwordSalt);

                        cmd.CommandText = "INSERT INTO users([email], [name], password_hash, password_salt, role, active) VALUES (@email, @name, @password_hash, @password_salt, @role,1)";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email_user ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar) { Value = body.name ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash });
                        cmd.Parameters.Add(new SqlParameter("@password_salt", SqlDbType.VarBinary) { Value = passwordSalt });
                        cmd.Parameters.Add(new SqlParameter("@role", SqlDbType.Int) { Value = body.role });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        string token = Guid.NewGuid().ToString();

                        cmd.CommandText = "Insert into token (token, expire_date, usage_type,email) values (@token,null,'reset',@email)";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email_user ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@token", SqlDbType.VarChar) { Value = token });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        string sendTo = body.email_user ?? "";
                        string subject = "Account Created";
                        string verificationUrl = baseUrl + "/new-password/" + token;

                        string emailBody = $@"
    <html>
    <head>
        <style>
            body {{
                font-family: 'Montserrat', sans-serif;
                background-color: #f5f5f5;
                margin: 0;
                padding: 0;
            }}

            .container {{
                max-width: 600px;
                margin: 0 auto;
                background-color: #ffffff;
                border-radius: 20px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                padding: 40px;
            }}

            .logo {{
                text-align: center;
                margin-bottom: 40px;
            }}

            .logo img {{
                max-width: 200px;
                height: auto;
            }}

            .logo h2 {{
                margin-top: 0;
            }}

            h2 {{
                color: #226957;
                font-size: 24px;
                margin-bottom: 20px;
            }}

            p {{
                color: #666666;
                font-size: 16px;
                line-height: 1.5;
                margin-bottom: 16px;
            }}

            table tr td {{
                color: #666666;
}}             

            a.button {{
                display: inline-block;
                padding: 12px 24px;
                background-color: #226957;
                color: #ffffff;
                text-decoration: none;
                border-radius: 4px;
                transition: background-color 0.3s ease;
            }}

            a.button:hover {{
                background-color: #EA9E1F;
            }}

            .footer {{
                margin-top: 40px;
                text-align: center;
                font-size: 14px;
                color: #999999;
            }}
        </style>
    </head>
    <body>
        <div class=""container"">
            <div class=""logo"">
                <img src=""https://i.imgur.com/rZtKzHH.png"" alt=""Company Logo"">
                <h2>D'Language</h2>             
            </div>
            <h2>Hello, {body.name}</h2>
            <p>Your account has been successfully created. Here are your credentials:</p>
            <table>
                <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td>{body.name}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>{body.email_user}</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>:</td>
                    <td><strong>{password}</strong></td>
                </tr>
            </table>
            <p>We recommend changing your password. Click the link below to reset your password:</p>
            <a class=""button"" href=""{verificationUrl}"">Reset password</a>
            <p class=""footer"">Best regards,<br>D'Language Team</p>
        </div>
    </body>
    </html>
";

                        EmailLogic.SendEmail(sendTo, subject, emailBody);

                        cmd.Transaction.Commit();
                        //conn.Close();

                        return "Success Add User";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static string EditUser(EditUserRequest body)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        if (String.IsNullOrEmpty(body.email_user))
                        {
                            throw new Exception("email_user cant be empty");
                        }

                        //if (String.IsNullOrEmpty(body.email_admin))
                        //{
                          //  throw new Exception("email_admin cant be empty");
                        //}


                        //string checkUserQuery = "Select top 1 * from users where [email] = @email and role = @role";
                        //SqlParameter[] sqlParams = new SqlParameter[]
                        //{
                          //  new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email_admin ?? "" },
                           // new SqlParameter("@role", SqlDbType.Int) { Value = 1 }
                        //};

                        //DataTable admin = CRUD.ExecuteQuery(checkUserQuery, sqlParams);

                        //if (admin.Rows.Count == 0)
                        //{
                          //  throw new Exception("Sorry! You are not authorized to perform this action");
                       // }

                        cmd.CommandText = "Update users set name = @name, role = @role, active = @active where email = @email";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email_user ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar) { Value = body.name ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@role", SqlDbType.Int) { Value = body.role });
                        cmd.Parameters.Add(new SqlParameter("@active", SqlDbType.Int) { Value = body.active });

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.Transaction.Commit();
                        conn.Close();

                        return "Success Edit User";
                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        conn.Close();
                        throw ex;
                    }
                }
            }
        }

    }
}
