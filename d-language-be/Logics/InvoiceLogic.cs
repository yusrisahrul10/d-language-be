﻿using d_language_be.Models.Request.Admin;
using d_language_be.Models.Request.Invoice;
using d_language_be.Models.Response.Invoice;
using Google.Protobuf.WellKnownTypes;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace d_language_be.Logics
{
    public class InvoiceLogic
    {
        private static string connectionString = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
        }


        public static string AddInvoice(Invoice body)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand  cmd = conn.CreateCommand())
                {
                    cmd.Connection = conn;
                    cmd.Transaction = conn.BeginTransaction();

                    try
                    {
                        if (string.IsNullOrEmpty(body.payment_method))
                        {
                            throw new Exception("Payment method is required.");
                        }

                        if (body.detail_invoice.Count == 0)
                        {
                            throw new Exception("Item can't be empty.");
                        }

                        string invoiceNo = "DLA";
                        
                        cmd.CommandText = "INSERT INTO invoice(email, total_qty, checkout_date, total_price, payment_method) VALUES (@email, @total_qty, @checkout_date, @total_price, @payment_method); select SCOPE_IDENTITY();";
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@total_qty", SqlDbType.Int) { Value = body.total_qty});
                        cmd.Parameters.Add(new SqlParameter("@checkout_date", SqlDbType.VarChar) { Value = body.checkout_date ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@total_price", SqlDbType.Int) { Value = body.total_price });
                        cmd.Parameters.Add(new SqlParameter("@payment_method", SqlDbType.VarChar) { Value = body.payment_method ?? "" });

                        decimal invoice_id = (decimal)cmd.ExecuteScalar();

                        cmd.Parameters.Clear();

                        cmd.CommandText = "Update invoice set invoice_no = @invoice_no where invoice_id = @invoice_id";
                        cmd.Parameters.Add(new SqlParameter("@invoice_no", SqlDbType.VarChar) { Value = invoiceNo + invoice_id.ToString("00000") ?? "" });
                        cmd.Parameters.Add(new SqlParameter("@invoice_id", SqlDbType.Int) { Value = invoice_id });
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        foreach (var invoicedetail in body.detail_invoice)
                        {
                            cmd.CommandText = "INSERT INTO invoice_detail (invoice_id, course_name, language_name, course_schedule, price) " +
                                "VALUES (@invoice_id, @course_name, @language_name, @course_schedule, @price);" +
                                "INSERT INTO my_class (email, course_img, language_name, course_name, schedule) " +
                                "VALUES (@email, @course_img, @language_name, @course_name, @course_schedule);";
                            cmd.Parameters.Add(new SqlParameter("@invoice_id", SqlDbType.Int) { Value = invoice_id });
                            cmd.Parameters.Add(new SqlParameter("@course_name", SqlDbType.VarChar) { Value = invoicedetail.course_name ?? "" });
                            cmd.Parameters.Add(new SqlParameter("@language_name", SqlDbType.VarChar) { Value = invoicedetail.language_name ?? "" });
                            cmd.Parameters.Add(new SqlParameter("@course_schedule", SqlDbType.VarChar) { Value = invoicedetail.course_schedule ?? "" });
                            cmd.Parameters.Add(new SqlParameter("@price", SqlDbType.Int) { Value = invoicedetail.price });
                            cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" });
                            cmd.Parameters.Add(new SqlParameter("@course_img", SqlDbType.VarChar) { Value = invoicedetail.course_img ?? "" });
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                        }

                        foreach (var checkout in body.detail_invoice)
                        {
                            cmd.CommandText = "delete from checkout where checkout_id = @checkout_id";
                            cmd.Parameters.Add(new SqlParameter("@checkout_id", SqlDbType.Int) { Value = checkout.checkout_id });
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }                        

                        cmd.Transaction.Commit();
                        return "Purchase Successfully";

                    }
                    catch (Exception ex)
                    {
                        cmd.Transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }

        public static List<InvoiceResponse> GetInvoiceByEmail(UserRequest body)
        {
            List <InvoiceResponse> result = new List<InvoiceResponse>();
            try
            {
                if (String.IsNullOrEmpty(body.email))
                {
                    throw new Exception("email cant be empty");
                }

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    String query = "select invoice_id, invoice_no, checkout_date, total_qty, total_price from invoice where email = @email";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" });
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            InvoiceResponse invoice = new InvoiceResponse()
                            {
                                invoice_id = Convert.ToInt32(dr["invoice_id"]),
                                invoice_no = dr["invoice_no"].ToString(),
                                date = dr["checkout_date"].ToString(),
                                total_course = Convert.ToInt32(dr["total_qty"]),
                                price = Convert.ToInt32(dr["total_price"]),
                            };
                            result.Add(invoice);
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static InvoiceDetailResponse GetDetailInvoiceById(UserInvoice body)
        {
            InvoiceDetailResponse result = null;
            
            try
            {

                if (String.IsNullOrEmpty(body.email))
                {
                    throw new Exception("email cant be empty");
                }

                if (String.IsNullOrEmpty(body.invoice_id.ToString()))
                {
                    throw new Exception("invoice_id cant be empty");
                }
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    //string query = "select course_name, language_name, course_schedule, price from invoice_detail where invoice_id = @id";
                    string query = "SELECT invoice.invoice_id, invoice_no, email, checkout_date, total_price," +
                        " course_name, language_name, course_schedule, price from invoice join" +
                        " invoice_detail on invoice.invoice_id = invoice_detail.invoice_id" +
                        " where invoice.invoice_id = @id and email = @email";


                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar) { Value = body.invoice_id });
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" });
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            if (result == null)
                            {
                                result = new InvoiceDetailResponse()
                                {
                                    invoice_no = (string)dr["invoice_no"],
                                    date = (string)dr["checkout_date"],
                                    total_price = (int)dr["total_price"],
                                    detail_invoice = new List<DetailInvoiceByInvoiceNo>()
                                };
                            }

                            result.detail_invoice.Add(new DetailInvoiceByInvoiceNo
                            {
                                course_name = dr["course_name"].ToString(),
                                language = dr["language_name"].ToString(),
                                schedule = dr["course_schedule"].ToString(),
                                price = Convert.ToInt32(dr["price"])
                            });
                            
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;

        }

        public static List<InvoiceAllResponse> GetAllInvoice()
        {
            Dictionary<string, InvoiceAllResponse> invoiceAll = new Dictionary<string, InvoiceAllResponse>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string query = "select email, invoice_id, invoice_no, total_qty, checkout_date, total_price from invoice";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            InvoiceAllResponse existingInvoiceAll;

                            if (!invoiceAll.TryGetValue((string)dr["email"], out existingInvoiceAll))
                            {
                                existingInvoiceAll = new InvoiceAllResponse
                                {
                                    email = (string)dr["email"],
                                    invoices = new List<InvoiceResponse>()
                                };
                                invoiceAll.Add((string)dr["email"], existingInvoiceAll);
                            }

                            existingInvoiceAll.invoices.Add(new InvoiceResponse
                            {
                                invoice_id = (int)dr["invoice_id"],
                                invoice_no = (string)dr["invoice_no"],
                                date = (string)dr["checkout_date"],
                                total_course = (int)dr["total_qty"],
                                price = (int)dr["total_price"]
                            });
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return invoiceAll.Values.ToList();
        }
    }
}
