﻿using d_language_be.Models;
using System.Data.SqlClient;

namespace d_language_be.Logics
{
    public class LanguageLogic
    {
        private static string connectionString = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
        }

        public static List<Language> GetLanguages()
        {
            List<Language> result = new List<Language>();

            try
            {
                using(SqlConnection conn  = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "select * from language";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                       SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Language language = new Language()
                            {
                                LanguageID = Convert.ToInt32(reader["language_id"]),
                                Name = reader["name"].ToString(),
                                Description = reader["description"].ToString(),
                                FlagImg = reader["flag_img"].ToString(),
                                BannerImg = reader["banner_img"].ToString()
                            };
                            result.Add(language);
                        }
                    }
                    conn.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public static List<Language> GetLanguage(string category)
        {
            List<Language> result = new List<Language>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "select top(1) * from language where name = @category";
                    
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@category", category);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Language language = new Language()
                            {
                                LanguageID = Convert.ToInt32(reader["language_id"]),
                                Name = reader["name"].ToString(),
                                Description = reader["description"].ToString(),
                                FlagImg = reader["flag_img"].ToString(),
                                BannerImg = reader["banner_img"].ToString()
                            };
                            result.Add(language);
                        }
                    }
                    conn.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
