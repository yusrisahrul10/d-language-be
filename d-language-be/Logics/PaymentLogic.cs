﻿using d_language_be.Models;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using MySqlX.XDevAPI.Common;
using System.Text;

namespace d_language_be.Logics
{
    public class PaymentLogic
    {
        private static string connectionString = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
        }

        public static List<Payment> GetPayment()
        {
            List<Payment> result = new List<Payment>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT * FROM payments";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            Payment payment = new Payment()
                            {
                                paymentsId = Convert.ToInt32(reader["payments_id"]),
                                name = reader["name"].ToString(),
                                paymentImg = reader["payment_img"].ToString(),
                                status = Convert.ToInt32(reader["status"])
                            };
                            result.Add(payment);
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<Payment> GetPaymentById(int Id)
        {
            List<Payment> result = new List<Payment>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT * FROM payments WHERE payments_id = @id";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlParameter parameter = new SqlParameter("@id", SqlDbType.Int);
                        parameter.Value = Id;
                        cmd.Parameters.Add(parameter);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Payment payment = new Payment()
                            {
                                paymentsId = Convert.ToInt32(reader["payments_id"]),
                                name = reader["name"].ToString(),
                                paymentImg = reader["payment_img"].ToString(),
                                status = Convert.ToInt32(reader["status"])
                            };
                            result.Add(payment);
                        }
                    }
                    conn.Close();

                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<Payment> GetPaymentActiveOnly()
        {
            List<Payment> result = new List<Payment>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "select * from payments where status = 1";

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Payment payment = new Payment()
                            {
                                paymentsId = Convert.ToInt32(reader["payments_id"]),
                                name = reader["name"].ToString(),
                                paymentImg = reader["payment_img"].ToString(),
                                status = Convert.ToInt32(reader["status"])
                            };
                            result.Add(payment);
                        }
                    }
                    conn.Close();

                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static string UpdatePayment(int id, Payment body)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    using (SqlTransaction transaction = conn.BeginTransaction())
                    {
                        try
                        {
                            string query;
                            SqlCommand command;

                            // Update query berdasarkan paymentImg value
                            if (string.IsNullOrEmpty(body.paymentImg))
                            {
                                query = "UPDATE payments SET name = @name, status = @status WHERE payments_id = @paymentsId";
                                command = new SqlCommand(query, conn, transaction);
                                command.Parameters.AddWithValue("@name", body.name);
                                command.Parameters.AddWithValue("@status", body.status);
                                command.Parameters.AddWithValue("@paymentsId", id);
                            }
                            else
                            {
                                query = "UPDATE payments SET name = @name, payment_img = @paymentImg, status = @status WHERE payments_id = @paymentsId";
                                command = new SqlCommand(query, conn, transaction);
                                command.Parameters.AddWithValue("@name", body.name);
                                command.Parameters.AddWithValue("@paymentImg", body.paymentImg);
                                command.Parameters.AddWithValue("@status", body.status);
                                command.Parameters.AddWithValue("@paymentsId", id);
                            }

                            command.ExecuteNonQuery();

                            transaction.Commit();

                            return "Payment Method has been update!";
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            Console.WriteLine($"Error : {ex.Message}");
                            throw;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error : {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error : {ex.Message}");
                throw;
            }
        }


        public static string AddPayments(Payment body)
        {
            try
            {
                if (string.IsNullOrEmpty(body.name))
                {
                    throw new Exception("Payment Name cannot be empty.");
                }

                if (string.IsNullOrEmpty(body.paymentImg))
                {
                    throw new Exception("Payment Image cannot be empty.");
                }

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string checkQuery = "SELECT COUNT(*) FROM payments WHERE name = @name";
                    string insertQuery = "INSERT INTO payments (payment_img, name, status) VALUES (@paymentImg, @name, @status); SELECT SCOPE_IDENTITY();";

                    using (SqlCommand checkCommand = new SqlCommand(checkQuery, connection))
                    {
                        checkCommand.Parameters.AddWithValue("@name", body.name);
                        int count = (int)checkCommand.ExecuteScalar();

                        if (count > 0)
                        {
                            throw new Exception("Payment Method has already been registered.");
                        }
                    }

                    using (SqlCommand insertCommand = new SqlCommand(insertQuery, connection))
                    {
                        insertCommand.Parameters.AddWithValue("@name", body.name);
                        insertCommand.Parameters.AddWithValue("@paymentImg", body.paymentImg);
                        insertCommand.Parameters.AddWithValue("@status", body.status);

                        insertCommand.ExecuteNonQuery();
                        return "New payment Method has been added.";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error : {ex.Message}");
                throw;
            }
        }







    }
}
