﻿using d_language_be.Models;
using System.Data;
using System.Data.SqlClient;

namespace d_language_be.Logics
{
    public class CourseLogic
    {
        private static string connectionString = "";
        private static string baseUrl = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
            baseUrl = configuration["fronturl"];
        }

        public static List<Course> GetCourses()
        {
            List<Course> result = new List<Course>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT top(6) course_id, c.name as course_name, l.name as category, c.description, c.price, c.course_img FROM course c JOIN language l ON c.language_id = l.language_id";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            Course course = new Course()
                            {
                                courseId = Convert.ToInt32(dr["course_id"]),
                                name = dr["course_name"].ToString(),
                                category = dr["category"].ToString(),
                                description = dr["description"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                courseImg = dr["course_img"].ToString()
                            };
                            result.Add(course);
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<Course> GetCourse(int id)
        {
            List<Course> result = new List<Course>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT course_id, c.name as course_name, l.name as category, c.description, c.price, c.course_img FROM course c JOIN language l ON c.language_id = l.language_id WHERE course_id = @id";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (!dr.HasRows)
                        {
                            Console.WriteLine("Course not found.");
                        }

                        while (dr.Read())
                        {
                            Course course = new Course()
                            {
                                courseId = Convert.ToInt32(dr["course_id"]),
                                name = dr["course_name"].ToString(),
                                category = dr["category"].ToString(),
                                description = dr["description"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                courseImg = dr["course_img"].ToString()
                            };
                            result.Add(course);
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<Course> GetCourseByCategory(string category)
        {
            List<Course> result = new List<Course>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT course_id, c.name as course_name, l.name as category, c.description, c.price, c.course_img FROM course c JOIN language l ON c.language_id = l.language_id WHERE l.name = @category";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@category", category);
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (!dr.HasRows)
                        {
                            Console.WriteLine("Course not found.");
                        }

                        while (dr.Read())
                        {
                            Course course = new Course()
                            {
                                courseId = Convert.ToInt32(dr["course_id"]),
                                name = dr["course_name"].ToString(),
                                category = dr["category"].ToString(),
                                description = dr["description"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                courseImg = dr["course_img"].ToString()
                            };
                            result.Add(course);
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }

        public static List<Course> GetCourseBetween(string category, int id)
        {
            List<Course> result = new List<Course>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    string query = "SELECT course_id, c.name as course_name, l.name as category, c.description, c.price, c.course_img FROM course c JOIN language l ON c.language_id = l.language_id WHERE l.name = @category AND course_id != @id";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@category", category);
                        cmd.Parameters.AddWithValue("@id", id);
                        SqlDataReader dr = cmd.ExecuteReader();

                        if (!dr.HasRows)
                        {
                            Console.WriteLine("Course not found.");
                        }

                        while (dr.Read())
                        {
                            Course course = new Course()
                            {
                                courseId = Convert.ToInt32(dr["course_id"]),
                                name = dr["course_name"].ToString(),
                                category = dr["category"].ToString(),
                                description = dr["description"].ToString(),
                                price = Convert.ToInt32(dr["price"]),
                                courseImg = dr["course_img"].ToString()
                            };
                            result.Add(course);
                        }
                    }
                    conn.Close();
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"SQL error occurred: {ex.Message}");
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occurred: {ex.Message}");
                throw;
            }

            return result;
        }
    }
}
