﻿using d_language_be.Models.Request.Admin;
using d_language_be.Models.Request.Invoice;
using d_language_be.Models.Response.MyClass;
using System.Data;
using System.Data.SqlClient;

namespace d_language_be.Logics
{
    public class MyClassLogic
    {
        private static string connectionString = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:Default"];
        }

        public static List<ClassResponse> GetClassByEmail(UserRequest body)
        {
            List<ClassResponse> result = new List<ClassResponse>();

            try
            {
                if (String.IsNullOrEmpty(body.email))
                {
                    throw new Exception("email cant be empty");
                }

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    String query = "select course_img, language_name, course_name, schedule from my_class where email = @email";
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = body.email ?? "" });
                        SqlDataReader dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            ClassResponse classResponse = new ClassResponse()
                            {
                                course_img = dr["course_img"].ToString(),
                                language_name = dr["language_name"].ToString(),
                                course_name = dr["course_name"].ToString(),
                                schedule = dr["schedule"].ToString()
                            };
                            result.Add(classResponse);
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
