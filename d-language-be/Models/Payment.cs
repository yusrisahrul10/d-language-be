﻿namespace d_language_be.Models
{
    public class Payment
    {
        public int paymentsId { get; set; }
        public string name { get; set; }
        public string paymentImg { get; set; }
        public int status { get; set; }
    }
}
