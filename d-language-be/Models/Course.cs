﻿namespace d_language_be.Models
{
    public class Course
    {
        public int courseId { get; set; }
        public string name { get; set; } = String.Empty;
        public string category { get; set; } = String.Empty;
        public string? description { get; set; }
        public int price { get; set; }

        public string courseImg { get; set; }
    }
}
