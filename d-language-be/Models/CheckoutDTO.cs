﻿namespace d_language_be.Models
{
    public class CheckoutDTO
    {
        public int checkoutId { get; set; }
        public string? courseName { get; set; }
        public int price { get; set; }
        public string? courseImg { get; set; }
        public string? category { get; set; }
        public string? email { get; set; }
        public string? scheduleDate { get; set; }
        public string? payyed_at { get; set; }
    }
}
