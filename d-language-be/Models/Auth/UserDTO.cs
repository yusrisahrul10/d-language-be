﻿namespace d_language_be.Models.Auth
{
    public class UserDTO
    {
        public string? name { get; set; }

        public string email { get; set; }

        public string password { get; set; }
    }
}
