﻿namespace d_language_be.Models
{
    public class Checkout
    {
        public int checkoutId { get; set; }
        public int courseId { get; set; }
        public string name { get; set; }
        public string? email { get; set; }
        public int qty { get; set; }
        public int price { get; set; }
        public string scheduleDate { get; set; }
        public string? payedAt { get; set; }
    }
}
