﻿namespace d_language_be.Models
{
    public class Language
    {
        public int LanguageID { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? FlagImg { get; set; }
        public string? BannerImg { get; set; }
    }
}
