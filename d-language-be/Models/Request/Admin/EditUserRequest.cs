﻿namespace d_language_be.Models.Request.Admin
{
    public class EditUserRequest
    {
        public string email_user { get; set; }

        public string name { get; set; }

        public int role { get; set; }

        public int active { get; set; }
    }
}
