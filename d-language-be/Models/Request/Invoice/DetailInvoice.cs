﻿namespace d_language_be.Models.Request.Invoice
{
    public class DetailInvoice
    {
        public int invoice_id { get; set; }

        public int checkout_id { get; set; }

        public string course_name { get; set; }

        public string language_name { get; set; }

        public string course_schedule { get; set; }

        public string course_img { get; set; }

        public int price { get; set; }

    }
}
