﻿namespace d_language_be.Models.Request.Invoice
{
    public class Invoice
    {
        public string email { get; set; }

        public int total_qty { get; set; }

        public string checkout_date { get; set; }

        public int total_price { get; set; }

        public string payment_method { get; set; }

        public List<DetailInvoice> detail_invoice { get; set; }

    }
}
