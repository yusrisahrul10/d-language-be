﻿namespace d_language_be.Models.Response.Admin
{
    public class UserResponse
    {
        public string email { get; set; }
        public string name { get; set; }
        public int role { get; set; }

        public int active { get; set; }
    }
}
