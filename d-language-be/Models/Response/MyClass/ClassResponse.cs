﻿namespace d_language_be.Models.Response.MyClass
{
    public class ClassResponse
    {
        public string course_img { get; set; }

        public string language_name { get; set; }

        public string course_name { get; set; }

        public string schedule { get; set; }
    }
}
