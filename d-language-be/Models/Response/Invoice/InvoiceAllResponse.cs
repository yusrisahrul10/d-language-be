﻿namespace d_language_be.Models.Response.Invoice
{
    public class InvoiceAllResponse
    {
        public string email { get; set; }
        public List<InvoiceResponse> invoices { get; set; }
    }
}
