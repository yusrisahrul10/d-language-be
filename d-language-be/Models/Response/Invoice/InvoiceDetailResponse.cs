﻿namespace d_language_be.Models.Response.Invoice
{
    public class InvoiceDetailResponse
    {
        public string invoice_no { get; set; }

        public string date { get; set; }

        public int total_price { get; set; }

        public List<DetailInvoiceByInvoiceNo> detail_invoice { get; set; }
}

    public class DetailInvoiceByInvoiceNo
    {
        public string course_name { get; set; }

        public string language { get; set; }

        public string schedule { get; set; }

        public int price { get; set; }
    }
}
