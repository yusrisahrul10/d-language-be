﻿namespace d_language_be.Models.Response.Invoice
{
    public class InvoiceResponse
    {
        public int invoice_id { get; set; }
        public string invoice_no { get; set; }

        public string date { get; set; }

        public int total_course { get; set; }

        public int price { get; set; }

    }
}
